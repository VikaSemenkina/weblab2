<?php
echo nl2br("1. Link access\n");
/* Imagine a lot of code here */
$very_bad_unclear_name = "15 chicken wings";
// Write your code here:
$order =& $very_bad_unclear_name;
$order = $order." and 20 goose feet";
// Don't change the line below
echo "\nYour order is: $very_bad_unclear_name.";

/////////////////////////

echo nl2br("\n \n 2. Numbers\n");
$a = 5;
echo nl2br("a = $a \n");
$b = -10.25;
echo nl2br("b = $b\n");
echo 6+6;
echo nl2br("\n");
$last_month = 1187.23;
$this_month = 1089.98;
echo "Last month was spent ". $last_month-$this_month. " more than this month";

/////////////////////////

echo nl2br("\n \n 3. Multiplication and division\n");
$num_languages = 4;
$months = 11;
$days = $months * 16;
$days_per_language = $days / $num_languages;
echo "Meg studied programming in each language for an average of ". $days_per_language . " days";

/////////////////////////

echo nl2br("\n \n 4. Degree\n");
echo 8**2;

/////////////////////////

echo nl2br("\n \n 5. Assignment operators\n");
$my_num = 36;
$answer = $my_num;
$answer += 2;
$answer *= 2;
$answer -= 2;
$answer /= 2;
$answer -= $my_num;
echo $answer;

/////////////////////////

echo nl2br("\n \n 6. Math functions\n");
$a = 10;
$b = 3;
echo "a mod b = ".$a % $b.nl2br("\n");
if($a % $b == 0)
	echo "Divided";
else
	echo "Divided with the remainder, ". $a % $b.nl2br("\n");
$st = pow(2,10);
$kr = sqrt(245);
echo $st." ".$kr.nl2br("\n");
$M = array(4,2,5,19,13,0,10);
$sum = 0;
foreach ($M as &$value)
{
	$sum += $value**2;
}
echo "Square root of sum of squares of array elements ".sqrt($sum).nl2br("\n");
echo round(sqrt(379))." ".round(sqrt(379),1)." ".round(sqrt(379),2).nl2br("\n");
$OKR = array(
	"floor_okr" => floor(sqrt(587)),
	"ceil_okr" => ceil(sqrt(587)),
);
var_dump($OKR);
echo nl2br("\n");
$M = array(4, -2, 5, 19, -130, 0, 10);
echo "MIN = ". min($M). " MAX = ". max($M).nl2br("\n");
echo "Random: ". rand(1,100).nl2br("\n");
$M = array(0,0,0,0,0,0,0,0,0,0);
foreach ($M as &$value)
{
	$value = rand(1,50);
	echo $value. " ";
}
echo nl2br("\n");
$a = -10;
$b = 3;
echo "abs(a - b) = ".abs($a-$b).nl2br("\n");
$M = array(1, 2, -1, -2, 3, -3);
foreach ($M as &$value)
{
	$value = abs($value);
	echo $value. " ";
}
echo nl2br("\n");
$ch = 44;
$DEL =array();
echo "Divisors of $ch: ";
for($i = 1;$i<$ch+1;$i++)
{
	if($ch % $i == 0)
	{
		array_push($DEL,$i);
		echo $i. " ";
	}
}
echo nl2br("\n");
$M = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$nom = 0;
$ch = 0;
foreach ($M as &$value)
{
	$ch += $value;
	$nom += 1;
	if($ch>10)
	{
		echo "To get a number greater than 10, you need to add the first $nom numbers.".nl2br("\n");
		break;
	}
}

/////////////////////////

echo nl2br("\n \n 7. Functions\n");
function printStringReturnNumber()
{
	echo "Number: ";
	return 101;
}
$my_num=printStringReturnNumber();
echo $my_num;

/////////////////////////

echo nl2br("\n \n 8. Functions\n");
function increaseEnthusiasm($str)
{
	return $str."!";
}
$a = increaseEnthusiasm("Hello, World");
echo $a;
echo nl2br("\n");
function repeatThreeTimes($str)
{
	return $str.$str.$str;
}
$a = repeatThreeTimes("Banana");
echo $a;
echo nl2br("\n");
$a = increaseEnthusiasm(repeatThreeTimes("Oh God "));
echo $a;
echo nl2br("\n");
function cut($str, $n)
{
	if(strlen($str)<=$n)
		return $str;
	$N="";
	for($i = 0;$i<$n;$i++)
	{
		$N .= $str[$i];
	}
	return $N;
}
$a = "Aaaaaaaaaaaaaaaaaaaaaaaa";
echo $a.nl2br("\n");
$a = cut($a,10);
echo $a.nl2br("\n");
function REC($M,$ch)
{
	if($ch >= count($M))
		return;
	echo $M[$ch]." ";
	$ch += 1;
	REC($M,$ch);
}
$M = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
REC($M,0);
echo nl2br("\n");
function Sum($num)
{
	$sum = 0;
	$n = strval($num);
	for($i=0;$i<strlen($n);$i++)
	{
		$sum += intval($n[$i]);
	}
	if($sum>9)
	{
		return Sum($sum);
	}
	else
	{
		return $sum;
	}
}
echo Sum(9999);

/////////////////////////

echo nl2br("\n \n 9. Arrays\n");
$M = array();
for($i=1;$i<12;$i++)
{
	$STR="";
	for($j=1;$j<$i;$j++)
	{
		$STR.="x";
	}
	array_push($M,$STR);
	echo $STR. " ";
}
echo nl2br("\n");
function arrayFill($num, $kol)
{
	$M = array();
	for($i=0;$i<$kol;$i++)
	{
		array_push($M,$num);
	}
	return $M;
}
$M1 = arrayFill(101, 7);
foreach ($M1 as &$value)
{
	echo $value. " ";
}
echo nl2br("\n");
$M2 =  array(array(1, 2, 3), array(4, 5), array(6));
$SUM = 0;
for($i=0; $i<count($M2); $i++)
{
	for($j=0; $j<count($M2[$i]); $j++)
	{
		$SUM += $M2[$i][$j];
	}
}
echo $SUM;
echo nl2br("\n");
$M3 =  array(array(), array(), array());
$ch = 1;
for($i=0; $i<count($M3); $i++)
{
	for($j=0; $j<3; $j++)
	{
		array_push($M3[$i],$ch);
		$ch += 1;
	}
}
for($i=0; $i<count($M3); $i++)
{
	for($j=0; $j<count($M3[$i]); $j++)
	{
		echo $M3[$i][$j]." ";
	}
	echo nl2br("\n");
}
$M4=array(2,5,3,9);
$result = ($M4[0]*$M4[1])+($M4[2]*$M4[3]);
echo "Result: ".$result.nl2br("\n");
$user=array(
	"name"=>"Ivan",
	"surname"=>"Ivanov",
	"patronymic"=>"Ivanovich",
);
echo $user["name"]." ".$user["surname"]." ".$user["patronymic"].nl2br("\n");
$date=array(
	"year"=>"2023",
	"month"=>"03",
	"day"=>"24",
);
echo $date["year"].".".$date["month"].".".$date["day"].nl2br("\n");
$arr = ['a', 'b', 'c', 'd', 'e'];
echo count($arr)." ".$arr[array_key_last($arr)]." ".$arr[array_key_last($arr)-1].nl2br("\n");

/////////////////////////

echo nl2br("\n \n 10. IF-ELSE\n");
function SUMM($a,$b)
{
	if($a+$b>10)
		return "true";
	else 
		return "false";
}
$a = 10;
$b = 1;
echo $a." ".$b." ".nl2br("\n");
$BUL = SUMM($a,$b);
echo $BUL." ";
function RAVN($a,$b)
{
	if($a==$b)
		return "true";
	else 
		return "false";
}
$BUL = RAVN($a,$b);
echo $BUL." ";
echo nl2br("\n");
$test = 0;
if ($test == 0) 
	echo 'true';
echo nl2br("\n");
$age = 54;
if($age<10 or $age>99)
	echo "Number age less than 10 or greater than 99";
else
{
	$sum = 0;
	$n = strval($age);
	for($i=0;$i<strlen($n);$i++)
	{
		$sum += intval($n[$i]);
	}
	if($sum <= 9)
		echo "The sum of the digits is unambiguous".nl2br("\n");
	else
		echo "The sum of the digits is two digits".nl2br("\n");
}
$arr = array(4, -2, 5);
if(count($arr)==3)
{
		echo "Array has 3 elements, ";
		$summ = 0;
		for($i=0; $i<count($arr); $i++)
		{
			$summ+=$arr[$i];
		}
		echo "summ = ".$summ.nl2br("\n");
}
else 
	echo "Array does not have 3 elements".nl2br("\n");

/////////////////////////

echo nl2br("\n \n 11. Cycles\n");
$str = "x";
for($i=0; $i<20; $i++)
{
	echo $str.nl2br("\n");
	$str.="x";
}

/////////////////////////

echo nl2br("\n \n 12. Function combination\n");
function SR_ARF($M, $sum, $nom)
{
	if($nom == count($M))
	{
		echo "Arithmetic mean = ".$sum/count($M).nl2br("\n");
		return $sum;
	}
	$sum += $M[$nom];
	$nom += 1;
	SR_ARF($M, $sum, $nom);
}
$M = array(4, -2, 5, 19, -130, 0, 10);
SR_ARF($M,0,0);
function SUMMCH($i, $max, $summ)
{
	if($i ==$max+1)
	{
		echo "SUMM = ".$summ.nl2br("\n");
		return $summ;
	}
	$summ += $i;
	$i += 1;
	
	SUMMCH($i, $max, $summ);
}
SUMMCH(1,100,0);
$MM = array_combine(range('a', 'z'), range(1, 26));
print_r($MM);
echo nl2br("\n");
$A = "1234567890";
function INTE($n)
{
	return intval($n);
}
function DVA($A)
{
	$A = str_split($A, 2);
	$A = array_map('INTE',$A);
	$sum = array_sum($A);
	return $sum;
}
echo "12+34+56+78+90 = ".DVA($A);
?>